# Idevlab Essential (PHP Functions)

---

[![Auteur](https://img.shields.io/badge/auteur-%40fsinama-blueviolet?style=for-the-badge)](https://gitlab.com/FSinama)
[![Release](https://img.shields.io/gitlab/v/release/Idevlab/Helpers?include_prereleases&style=for-the-badge)]()
[![Codacy grade](https://img.shields.io/codacy/grade/36ddbd5c99314e8e8ec2465cf90865c5?style=for-the-badge)](https://www.codacy.com/gl/idevlab/Helpers/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=idevlab/Helpers&amp;utm_campaign=Badge_Grade)
[![License](https://img.shields.io/gitlab/license/Idevlab/Helpers?style=for-the-badge)](https://choosealicense.com/licenses/gpl-3.0/)
[![Téléchargement](https://img.shields.io/packagist/dt/Idevlab/Helpers?style=for-the-badge)](https://packagist.org/packages/idevlab/helpers)

Many PHP functions used in my projects, that you can use in your developments.

## Authors

[@fsinama](https://gitlab.com/FSinama)

## Installation

Install in your project with composer.

```bash
  composer require Idevlab/Helpers
```

--- 

## Methods

### ArrayHelper

#### ::merge(array...$arrays): array

Merge array

````php
/** @var array $foo */
$foo = ['one' => 1, 'three' => 3];

/** @var array $bar */
$bar = ['two' => 2];

$foo = ArrayHelper::merge($foo, $bar); // ['one' => 1, 'three' => 3, 'two' => 2]}
````

### StringHelper

#### ::toSnakeCase(string $string): string

Format a string in snake_case.

````php
/** @var string $foo */
$foo = 'Test_DeChaine';

$foo = StringHelper::toSnakeCase($foo); // test_de_chaine
````

#### ::toPascalCase(string $string): string

Format a string in PascalCase.

````php
/** @var string $foo */
$foo = 'Test_DeChaine';

$foo = StringHelper::toPascalCase($foo); // TestDeChaine
````

#### ::toSkewerCase(string $string): string

Format a string in skewer-case.

````php
/** @var string $foo */
$foo = 'Test_DeChaine';

$foo = StringHelper::toSkewerCase($foo); // test-de-chaine
````

#### ::toNoCase(string $string): string

Format a string in no case.

````php
/** @var string $foo */
$foo = 'Test_DeChaine';

$foo = StringHelper::toNoCase($foo); // test de chaine
````

#### ::toCase(string $string): string

Format a string in no case.

````php
/** @var string $foo */
$foo = 'Test_DeChaine';

// NO : 0
// PASCAL : 1
// CAMEL : 2
// SKEWER : 3
// SNAKE : 4

$foo = StringHelper::toCase($foo,1); // test de chaine
````

### JsonHelper

#### ::jsonSerialize(object $object, string ...$excludes): array

Serialize a object you.

````php

class Foo {
    public string $bar = 'purple';
    public string $other = 'exclude value';
}

$foo = new Foo();

$foo = JsonHelper::jsonSerialize($foo,['other']); // { 'bar' => 'purple' }
````

### DateHelper

#### ::randomDateForRange(DateTime $start, ?DateTime $end = new DateTime('now')): DateTime

Find a random date between $start and $end

---

## License

[GPL3 or later](https://choosealicense.com/licenses/gpl-3.0/)