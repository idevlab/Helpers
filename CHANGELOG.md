# Change Log

All notable changes made to this project are documented in this file.

## [1.0.2] - 2022-10-30

### Info

Update badge

## [1.0.1] - 2022-10-30

### Fix

Update readme.md

## [1.0.0] - 2022-10-30

### Add

Initialization and publication of library