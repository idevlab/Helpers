<?php
/**
 * This file is part of Idevlab essential.
 *
 * @license   https://opensource.org/licenses/MIT MIT License
 * @copyright 2022 Florian SINAMA
 * @author    Florian SINAMA <https://gitlab.com/FSinama>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code, to the root.
 */

declare(strict_types=1);

namespace Idevlab\Helpers;

/**
 * Class JsonHelper.
 *
 * @package Idevlab\Helpers
 */
final class JsonHelper
{
    /**
     * Serialize a object
     *
     * @param object $object
     * @param string ...$excludes
     *
     * @return array
     */
    final public static function jsonSerialize(object $object, string ...$excludes): array
    {
        $class_methods = get_class_methods($object);
        $array = [];

        foreach ($class_methods as $method) {
            if (str_starts_with($method, 'get')) {
                $attribute = lcfirst(substr($method, 3));
                if (!in_array($attribute, $excludes)) {
                    $array[$attribute] = $object->$method();
                }
            }
        }
        return $array;
    }
}