<?php

declare(strict_types=1);

namespace Idevlab\Helpers\Enumeration;

/**
 * Enumeration CaseFormat
 *
 * @package Idevlab\Essential\Enumeration
 */
enum CaseFormat: int
{
    case NO = 0;
    case PASCALE = 1;
    case CAMEL = 2;
    case SKEWER = 3;
    case SNAKE = 4;
}