<?php
/**
 * This file is part of Idevlab essential.
 *
 * @license   https://opensource.org/licenses/MIT MIT License
 * @copyright 2022 Florian SINAMA
 * @author    Florian SINAMA <https://gitlab.com/FSinama>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code, to the root.
 */

declare(strict_types=1);

namespace Idevlab\Helpers;

use Exception;
use Idevlab\Helpers\Exception\HelpersException;
use Throwable;

/**
 * Class ArrayHelper.
 *
 * @package Idevlab\Helpers
 */
final class ArrayHelper
{
    /**
     * Merge array
     *
     * @param array ...$arrays
     *
     * @return array
     */
    final public static function merge(array...$arrays): array
    {
        $merged = [];

        foreach ($arrays as $array) {
            foreach ($array as $key => $value) {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }

    /**
     * Apply callable on array's element.
     *
     * @param array     $array
     * @param callable  $callback
     * @param Exception $exception
     *
     * @return bool
     * @throws HelpersException
     * @throws $exception
     */
    final public static function validate(
        array     $array,
        callable  $callback,
        Exception $exception = new HelpersException()
    ): bool
    {
        try {
            array_walk($array, function (&$element) use ($exception, $callback, $array) {
                if (!$callback($element)) {
                    throw $exception;
                }
            });

            return true;
        } catch (Throwable $exception) {
            throw new HelpersException(previous: $exception);
        }
    }

    /**
     * Apply callable on array's element.
     *
     * @param array    $array
     * @param callable $callback
     *
     * @return array
     * @throws HelpersException
     */
    final public static function apply(array $array, callable $callback): array
    {
        try {
            array_walk($array, function (&$element) use ($callback, $array) {
                $element = $callback($element);
            });

            return $array;
        } catch (Throwable $exception) {
            throw new HelpersException(previous: $exception);
        }
    }

    /**
     * Apply callable on string array's element.
     *
     * @param array    $array
     * @param callable $callback
     *
     * @return array
     * @throws HelpersException
     */
    final public static function strApply(array $array, callable $callback): array
    {
        try {
            foreach ($array as &$element) {
                if (!is_string($element)) {
                    continue;
                }

                $element = $callback($element);
            }

            return $array;
        } catch (Throwable $exception) {
            throw new HelpersException(previous: $exception);
        }
    }

    /**
     * Apply callable on int array's element.
     *
     * @param array    $array
     * @param callable $callback
     *
     * @return array
     * @throws HelpersException
     */
    final public static function intApply(array $array, callable $callback): array
    {
        try {
            foreach ($array as &$element) {
                if (!is_int($element)) {
                    continue;
                }

                $element = $callback($element);
            }

            return $array;
        } catch (Throwable $exception) {
            throw new HelpersException(previous: $exception);
        }
    }
}