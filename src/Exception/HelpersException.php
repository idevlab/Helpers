<?php

declare(strict_types=1);

namespace Idevlab\Helpers\Exception;

use Exception;

/**
 * Class HelpersException
 *
 * @package Idevlab\Helpers\Exception
 */
class HelpersException extends Exception
{
    public const UNSUPPORTED_CASE = 1;
}