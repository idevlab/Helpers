<?php
/**
 * This file is part of Idevlab essential.
 *
 * @license   https://opensource.org/licenses/MIT MIT License
 * @copyright 2022 Florian SINAMA
 * @author    Florian SINAMA <https://gitlab.com/FSinama>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code, to the root.
 */

declare(strict_types=1);

namespace Idevlab\Helpers;

use Idevlab\Helpers\Enumeration\CaseFormat;
use Idevlab\Helpers\Exception\HelpersException;

/**
 * Class StringHelper.
 *
 * @package Idevlab\Helpers
 */
final class StringHelper
{
    private const REGEX_EXPLOSE = '!([A-Z][A-Z\d]*(?=$|[A-Z][a-z\d])|[A-Za-z][a-z\d]+)!';

    /**
     * Convert String format in no case.
     *
     * @param string     $string
     * @param CaseFormat $case
     *
     * @return string
     * @throws HelpersException
     */
    final public static function toCase(string $string, CaseFormat $case): string
    {
        return match ($case) {
            CaseFormat::NO      => StringHelper::toNoCase($string),
            CaseFormat::PASCALE => StringHelper::toPascalCase($string),
            CaseFormat::CAMEL   => StringHelper::toCamelCase($string),
            CaseFormat::SKEWER  => StringHelper::toSkewerCase($string),
            CaseFormat::SNAKE   => StringHelper::toSnakeCase($string),
        };
    }

    /**
     * Convert String format in no case.
     *
     * @param string $string
     *
     * @return string
     */
    final public static function toNoCase(string $string): string
    {
        $explodedString = StringHelper::explode($string);

        return implode(' ', $explodedString);
    }

    /**
     * Explodes the string based on a regex.
     *
     * @param string $string
     * @param string $regex default value : !([A-Z][A-Z\d]*(?=$|[A-Z][a-z\d])|[A-Za-z][a-z\d]+)!
     *
     * @return array
     */
    final public static function explode(string $string, string $regex = StringHelper::REGEX_EXPLOSE): array
    {
        $matches = [];

        preg_match_all($regex, $string, $matches);

        return $matches[0];
    }

    /**
     * Convert String format in PascalCase.
     *
     * @param string $string
     *
     * @return string
     * @throws HelpersException
     */
    final public static function toPascalCase(string $string): string
    {
        $explodedString = StringHelper::explode($string);
        $explodedString = ArrayHelper::strApply($explodedString, 'ucfirst');

        return implode('', $explodedString);
    }

    /**
     * Convert String format in camelCase.
     *
     * @param string $string
     *
     * @return string
     * @throws HelpersException
     */
    final public static function toCamelCase(string $string): string
    {
        $string = StringHelper::toPascalCase($string);

        return lcfirst($string);
    }

    /**
     * Convert String format in skewer-case.
     *
     * @param string $string
     *
     * @return string
     * @throws HelpersException
     */
    final public static function toSkewerCase(string $string): string
    {
        $explodedString = StringHelper::explode($string);
        $explodedString = ArrayHelper::strApply($explodedString, 'strtolower');

        return implode('-', $explodedString);
    }

    /**
     * Convert String in snake_case.
     *
     * @param string $string
     *
     * @return string
     * @throws HelpersException
     */
    final public static function toSnakeCase(string $string): string
    {
        $explodedString = StringHelper::explode($string);
        $explodedString = ArrayHelper::strApply($explodedString, 'strtolower');

        return implode('_', $explodedString);
    }
}