<?php
/**
 * This file is part of Idevlab essential.
 *
 * @license   https://opensource.org/licenses/MIT MIT License
 * @copyright 2022 Florian SINAMA
 * @author    Florian SINAMA <https://gitlab.com/FSinama>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code, to the root.
 */

declare(strict_types=1);

namespace Idevlab\Helpers;

use DateTime;

/**
 * Class DateHelper.
 *
 * @package Idevlab\Helpers
 */
final class DateHelper
{
    /**
     * Find a random date between $start and $end
     *
     * @param DateTime $start
     * @param DateTime $end
     *
     * @return DateTime
     */
    final public static function randomDateForRange(DateTime $start, DateTime $end): DateTime
    {
        $randomTimestamp = mt_rand($start->getTimestamp(), $end->getTimestamp());
        $randomDate = new DateTime();
        $randomDate->setTimestamp($randomTimestamp);
        return $randomDate;
    }
}